package com.example.springbatchcompleteproject;

import com.example.springbatchcompleteproject.entities.ProductVenderEntity;
import com.example.springbatchcompleteproject.entities.VendorEntity;

public class JoinedDataDTO {

    private VendorEntity vendorEntity;
    private ProductVenderEntity productVenderEntity;

    public JoinedDataDTO(VendorEntity vendorEntity, ProductVenderEntity productVenderEntity) {
    }

    public VendorEntity getVendorEntity() {
        return vendorEntity;
    }

    public void setVendorEntity(VendorEntity vendorEntity) {
        this.vendorEntity = vendorEntity;
    }

    public ProductVenderEntity getProductVenderEntity() {
        return productVenderEntity;
    }

    public void setProductVenderEntity(ProductVenderEntity productVenderEntity) {
        this.productVenderEntity = productVenderEntity;
    }
}


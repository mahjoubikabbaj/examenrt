package com.example.springbatchcompleteproject;

import com.example.springbatchcompleteproject.entities.ProductVenderEntity;
import com.example.springbatchcompleteproject.entities.VendorEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Component
public class KafkaConsumerService {

    @KafkaListener(topics = "read-from-AdventureWorks2012", groupId = "your-group-id")
    public void consumeMessages(List<JoinedDataDTO> joinedDataList) {
        // Write data to CSV file
        writeToCsv(joinedDataList);
    }

    private void writeToCsv(List<JoinedDataDTO> joinedDataList) {
        try (FileWriter csvWriter = new FileWriter("output.csv")) {
            for (JoinedDataDTO joinedDataDTO : joinedDataList) {
                VendorEntity vendorEntity = joinedDataDTO.getVendorEntity();
                ProductVenderEntity productVenderEntity = joinedDataDTO.getProductVenderEntity();

                // Replace getSomeField with the actual getter method for the field you want
                csvWriter.append(vendorEntity.getVendorName());
                csvWriter.append(",");
                csvWriter.append(productVenderEntity.getProductName()); // Replace with the actual method
                csvWriter.append("\n");
            }
            csvWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
            // Handle the exception according to your application's requirements
        }
    }}
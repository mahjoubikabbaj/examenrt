package com.example.springbatchcompleteproject;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KafkaProducerService {

    private final KafkaTemplate<String, JoinedDataDTO> kafkaTemplate;

    @Autowired
    public KafkaProducerService(KafkaTemplate<String, JoinedDataDTO> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendDataToKafkaTopic(List<JoinedDataDTO> joinedDataList) {
        for (JoinedDataDTO joinedDataDTO : joinedDataList) {
            // Assuming "your-topic" is the name of your Kafka topic
            // Use a suitable criterion for partitioning, replace "your-criterion" with your actual criterion
            String partitionKey = joinedDataDTO.getVendorEntity().getVendorName();
            kafkaTemplate.send("your-topic", partitionKey, joinedDataDTO);
        }
    }
}
package com.example.springbatchcompleteproject.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Product", schema = "Production")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProductID")
    private Long productId;

    @ManyToOne
    @JoinColumn(name = "CategoryID")
    private ProductCategoryEntity category;

    @Column(name = "ProductName")
    private String productName;

    // Other fields and getters/setters...
}
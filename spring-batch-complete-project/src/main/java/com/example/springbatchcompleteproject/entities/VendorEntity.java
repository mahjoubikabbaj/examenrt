package com.example.springbatchcompleteproject.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Vendor", schema = "Purchasing")
public class VendorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "VendorID")
    private Long vendorId;

    @Column(name = "VendorName")
    private String vendorName;

    // Other fields and getters/setters...
}
package com.example.springbatchcompleteproject.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ProductVender", schema = "Purchasing")
public class ProductVenderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProductVendorID")
    private Long productVendorId;


    private String productName;

    @ManyToOne
    @JoinColumn(name = "VendorID")
    private VendorEntity vendor;

    @ManyToOne
    @JoinColumn(name = "ProductID")
    private ProductEntity product;

    public void setSomeNumericField(String convertedToString) {
    }

    // Other fields and getters/setters...
}
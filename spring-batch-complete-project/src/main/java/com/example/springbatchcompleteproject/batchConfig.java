package com.example.springbatchcompleteproject;



import com.example.springbatchcompleteproject.items.EntityItemProcessor;
import com.example.springbatchcompleteproject.items.EntityItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.IOException;
import java.util.List;

@Configuration
@EnableBatchProcessing
public class batchConfig {

    @Autowired
    private  JobRepository jobRepository;
    @Autowired
    private  PlatformTransactionManager platformTransactionManager;



    @Autowired
    private ItemReader<List<JoinedDataDTO>> itemReader;

    @Autowired
    private ItemProcessor<List<JoinedDataDTO>, List<JoinedDataDTO>> itemProcessor;

    @Autowired
    private ItemWriter<List<JoinedDataDTO>> itemWriter;

    @Bean
    @StepScope
    public EntityItemReader yourEntityItemReader() {
        return new EntityItemReader();
    }

    @Bean
    public ItemProcessor<List<JoinedDataDTO>, List<JoinedDataDTO>> yourEntityItemProcessor() {
        return new EntityItemProcessor();
    }

    @Bean
    public Job detectAlteredLinesJob(Step detectAlteredLinesStep) {
        return new JobBuilder("detectAlteredLinesJob", jobRepository)
                .incrementer(new RunIdIncrementer())
                .flow(detectAlteredLinesStep)
                .end()
                .build();
    }

    @Bean
    public Step detectAlteredLinesStep() {
        return new StepBuilder("detectAlteredLinesStep", jobRepository)
                .<List<JoinedDataDTO>, List<JoinedDataDTO>>chunk(5,platformTransactionManager)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter
                .faultTolerant()
                .retryLimit(3)
                .retry(Exception.class)
                .noRetry(IOException.class)
                .build());
    }
}
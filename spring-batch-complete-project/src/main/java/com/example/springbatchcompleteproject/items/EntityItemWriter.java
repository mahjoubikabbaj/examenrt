package com.example.springbatchcompleteproject.items;

import com.example.springbatchcompleteproject.JoinedDataDTO;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EntityItemWriter implements ItemWriter<List<JoinedDataDTO>> {

    public void write(List<? extends List<JoinedDataDTO>> items) throws Exception {


        for (List<JoinedDataDTO> joinedDataList : items) {
            writeToCsv(joinedDataList);
        }
    }

    private void writeToCsv(List<JoinedDataDTO> joinedDataList) {
    // Write to CSV file
    }
}}

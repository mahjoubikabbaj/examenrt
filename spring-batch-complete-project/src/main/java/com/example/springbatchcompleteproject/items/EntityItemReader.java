package com.example.springbatchcompleteproject.items;

import com.example.springbatchcompleteproject.JoinedDataDTO;
import com.example.springbatchcompleteproject.entities.ProductVenderEntity;
import com.example.springbatchcompleteproject.entities.VendorEntity;
import org.springframework.batch.item.ItemReader;

import java.util.ArrayList;
import java.util.List;

public class EntityItemReader implements ItemReader<JoinedDataDTO> {

    @Override
    public JoinedDataDTO read() throws Exception {
        // Example: Fetch data from the database and perform joins
        List<JoinedDataDTO> joinedDataList = new ArrayList<>();


        // Add sample data for demonstration purposes
        VendorEntity vendorEntity = new VendorEntity();
        vendorEntity.setVendorName("Vendor 1");

        ProductVenderEntity productVenderEntity = new ProductVenderEntity();
        productVenderEntity.setProductName("Product 1");

        JoinedDataDTO joinedDataDTO = new JoinedDataDTO(vendorEntity, productVenderEntity);
        joinedDataList.add(joinedDataDTO);

        return (JoinedDataDTO) joinedDataList;
    }
}


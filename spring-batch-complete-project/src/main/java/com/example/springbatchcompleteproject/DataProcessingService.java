package com.example.springbatchcompleteproject;

import com.example.springbatchcompleteproject.entities.ProductVenderEntity;
import com.example.springbatchcompleteproject.entities.VendorEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class DataProcessingService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private DataTransformationService transformationService;

    @Autowired
    private KafkaProducerService kafkaProducerService;

    public void processAndSendData() {
        // Step a: Connect to the database and fetch necessary data
        List<VendorEntity> vendorEntities = fetchDataFromDatabase();

        // Step b: Create a transformation to join data
        List<JoinedDataDTO> joinedDataList = transformToJoinedData(vendorEntities);

        // Step c: Create a transformation to alter data randomly
        transformationService.alterDataRandomly(joinedDataList);

        // Step d: Save data in partitions of a Kafka topic
        saveDataToKafkaTopic(joinedDataList);
    }

    private List<VendorEntity> fetchDataFromDatabase() {
        return entityManager.createQuery("SELECT v FROM VendorEntity v", VendorEntity.class).getResultList();
    }

    private List<JoinedDataDTO> transformToJoinedData(List<VendorEntity> vendorEntities) {

        return vendorEntities.stream()
                .map(vendorEntity -> {
                    JoinedDataDTO joinedDataDTO = new JoinedDataDTO();
                    joinedDataDTO.setVendorEntity(vendorEntity);

                    joinedDataDTO.setProductVenderEntity(fetchProductVenderForVendor(vendorEntity));
                    return joinedDataDTO;
                })
                .toList();
    }

    private ProductVenderEntity fetchProductVenderForVendor(VendorEntity vendorEntity) {
        // Implement logic to fetch related ProductVenderEntity
        // This is a placeholder, replace it with your actual logic
        return new ProductVenderEntity();
    }

    private void saveDataToKafkaTopic(List<JoinedDataDTO> joinedDataList) {
        // Assuming you have a Kafka topic named "your-topic"
        // You may adjust this based on your actual Kafka setup
        kafkaProducerService.sendDataToKafkaTopic(joinedDataList);
    }
}
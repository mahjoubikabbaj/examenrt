package com.example.springbatchcompleteproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchCompleteProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchCompleteProjectApplication.class, args);
	}

}

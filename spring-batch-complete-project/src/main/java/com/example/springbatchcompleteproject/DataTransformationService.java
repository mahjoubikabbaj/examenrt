package com.example.springbatchcompleteproject;

import com.example.springbatchcompleteproject.entities.ProductVenderEntity;
import com.example.springbatchcompleteproject.entities.VendorEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class DataTransformationService {

    public void alterDataRandomly(List<JoinedDataDTO> joinedDataList) {
        Random random = new Random();

        for (JoinedDataDTO joinedDataDTO : joinedDataList) {
            VendorEntity vendorEntity = joinedDataDTO.getVendorEntity();
            ProductVenderEntity productVenderEntity = joinedDataDTO.getProductVenderEntity();

            // Randomly alter less than 5% of a column with invalid data
            if (random.nextDouble() < 0.05) {
                vendorEntity.setVendorName("InvalidVendorName");
            }

            // Randomly change the data type of 10% of a column
            if (random.nextDouble() < 0.1) {
                productVenderEntity.setSomeNumericField("ConvertedToString");
            }

            // Add more random alterations as needed
        }
    }
}